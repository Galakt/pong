﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Superior : MonoBehaviour
{

    private Transform playerTransform;

    public Transform ballPosition;

    public float speed;
    private float move;

    void Start()
    {
        playerTransform = transform;

    }

    void Update()
    {
        if (ballPosition.position.y > 0)
        {
            if (ballPosition.position.x > playerTransform.position.x)
            {
                move = 1;
            }
            else
            {
                move = -1;
            }


            move *= Time.deltaTime;
            move *= speed;
            transform.Translate(move, 0, 0);

            if (transform.position.x < -8.0f)
            {
                transform.position = new Vector3(-8.0f, playerTransform.position.y, transform.position.z);
            }
            else if (transform.position.x > 8.0f)
            {
                transform.position = new Vector3(8.0f, playerTransform.position.y, transform.position.z);
            }
        }
    }
}
