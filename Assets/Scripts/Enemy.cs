﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{



    public Transform ballPosition;

    public float speed;
    private float move;

    void Start()
    {
        // Equivalente a playerTransform = GetComponent<Transform> ();

    }

    void Update()
    {
        if (ballPosition.position.x > 0)
        {
            if (ballPosition.position.y > transform.position.y)
            {
                move = 1;
            }
            else
            {
                move = -1;
            }
            move *= Time.deltaTime;
            move *= speed;
            transform.Translate(0, move, 0);

            if (transform.position.y < -4.0f)
            {
                transform.position = new Vector3(transform.position.x, -4.0f, transform.position.z);
            }
            else if (transform.position.y > 4.0f)
            {
                transform.position = new Vector3(transform.position.x, 4.0f, transform.position.z);
            }
        }
    }
}

