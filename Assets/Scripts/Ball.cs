﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;

	// Use this for initialization
	void Start () 
	{
        int randX, randY;
        if (Random.value > 0.5f) randX = -1;
        else randX = 1;
        if (Random.value > 0.5f) randY = -1;
        else randY = 1;
        iniSpeed = speed = new Vector2(Random.Range(3,8)*randX,Random.Range(3,8)*randY);
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Bounds") 
		{
			speed.y *= -1.15f;
		}
		if (other.tag == "Player") 
		{
			speed.x *= -1.20f;
		} 
		else if (other.tag == "Goal") 
		{
			Reset ();
		}
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
