﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {



	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();

	}

	void Update () 
	{
		/*
		//Read input
		move = Input.GetAxis("Vertical");


		//Apply speed
		move *= speed;


		//Limit position
		newPos = new Vector3 (newPos.x, playerTransform.position.y + move, newPos.z );

		if (newPos.y < -3.0f) 
		{
			newPos = new Vector3 (newPos.x, -3.0f, newPos.z);
		}
		if (newPos.y > 3.0f) 
		{
			newPos = new Vector3 (newPos.x, 3.0f, newPos.z);
		}

		//Set new position
		playerTransform.position = newPos;
		*/

		move = Input.GetAxis("Vertical") * speed;
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(0, move, 0);

		if (transform.position.y < -4.0f) {
			transform.position = new Vector3 (transform.position.x, -4.0f, transform.position.z);
		}else if(transform.position.y > 4.0f) {
			transform.position = new Vector3 (transform.position.x, 4.0f, transform.position.z);
		}
	}
}
